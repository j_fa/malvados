from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
app = Flask(__name__)

@app.route("/")
def main():
    html = """<!DOCTYPE html>
        <html>
            <head>
                <meta charset="utf-8"/>
                <title>Infinitos Malvados</title>
                <script src="static/js/source.js" type="text/javascript"></script> 
                <link type="text/css" rel="stylesheet" href="static/css/style.css"/>   
            </head>
        """
            
    html += """<body>
        <div class="infinitos-malvados" id="infinitos">
            <h1>Infinitos Malvados</h1>
            <script type="text/javascript">
                maisMalvados();
            </script>
        </div>
        """
        
    html += """<script type="text/javascript">
                window.onscroll = function(ev) {
                    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) maisMalvados();
                };
            </script>       
        </body>
        </html>
        """
    return html

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
