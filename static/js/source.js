function range(i) {
    return i>1 ? range(i-1).concat(i-1) : [];
}

function shuffle(a) {
    for (let i = a.length; i; i--) {
        let j = Math.floor(Math.random() * i);
        [a[i - 1], a[j]] = [a[j], a[i - 1]];
    }
}
var indexes = range(1714);
shuffle(indexes);
var begin = 0;
var limit = 30;

function maisMalvados(){
  var infinitos = document.getElementById('infinitos');
  for (var i = begin; i <= limit; i++) {
    var outroMalvados = document.createElement("div");
    outroMalvados.className = "um-malvados";
    var logoMalvados = document.createElement("img");
    logoMalvados.src = 'static/img/logo_' + indexes[i] + '.gif';
    outroMalvados.appendChild(logoMalvados);
    var imgMalvados = document.createElement("img");
    imgMalvados.src = 'static/img/tira_' + indexes[i] + '.gif';
    outroMalvados.appendChild(imgMalvados);
    infinitos.appendChild(outroMalvados);
  }
  if (limit > 1714){
    begin = 0;
    limit = 30;
  } else {
    begin = limit + 1;
    limit = limit + 30;
  }
}
